package com.roulette.ws.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

import com.roulette.ws.model.Roulette;
import com.roulette.ws.util.Constants;

@Repository
public class RouletteDao {
	@Autowired
	private RedisTemplate<String, ?> template;
	public int getLastRoulette() {
		return template.opsForHash().size(Constants.HK_ROULETTE).intValue();
	}
	public boolean createNewRoulette(Roulette roulette) {
		template.opsForHash().put(Constants.HK_ROULETTE, roulette.getRouletteId(), roulette);
		return true;
	}
	public Object getRouletteById(int rouletteId) {
		return template.opsForHash().get(Constants.HK_ROULETTE, rouletteId);
	}
	public boolean updateRoulette(Roulette roulette) {
		template.opsForHash().put(Constants.HK_ROULETTE, roulette.getRouletteId(), roulette);
		return true;
	}
	public List<Object> listAllRoulettes(){
		return template.opsForHash().values(Constants.HK_ROULETTE);
	}
}
