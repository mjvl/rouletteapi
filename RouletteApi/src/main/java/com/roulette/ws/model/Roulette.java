package com.roulette.ws.model;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;

@RedisHash("Roulette")
public class Roulette implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	private int rouletteId;
	private boolean rouletteOpen;
	private List<Bet> betList;
	public int getRouletteId() {
		return rouletteId;
	}
	public void setRouletteId(int rouletteId) {
		this.rouletteId = rouletteId;
	}
	public boolean isRouletteOpen() {
		return rouletteOpen;
	}
	public void setRouletteOpen(boolean rouletteOpen) {
		this.rouletteOpen = rouletteOpen;
	}
	public List<Bet> getBetList() {
		return betList;
	}
	public void setBetList(List<Bet> betList) {
		this.betList = betList;
	}
}
