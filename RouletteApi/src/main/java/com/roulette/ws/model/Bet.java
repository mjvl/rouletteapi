package com.roulette.ws.model;

import java.io.Serializable;

public class Bet implements Serializable {
	private static final long serialVersionUID = 1L;
	private int betId;
	private int userId;
	private String betNumber;
	private String color;
	private float moneyBet;
	private boolean winner;
	public int getBetId() {
		return betId;
	}
	public void setBetId(int betId) {
		this.betId = betId;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getBetNumber() {
		return betNumber;
	}
	public void setBetNumber(String betNumber) {
		this.betNumber = betNumber;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public float getMoneyBet() {
		return moneyBet;
	}
	public void setMoneyBet(float moneyBet) {
		this.moneyBet = moneyBet;
	}
	public boolean isWinner() {
		return winner;
	}
	public void setWinner(boolean winner) {
		this.winner = winner;
	}
}
