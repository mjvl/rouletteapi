package com.roulette.ws.util;

public class Constants {
	//public static final int ZERO = 0;
	public static final String HK_ROULETTE = "Roulette";
	public static final String PATH = "/roulette/roulette-api";
	public static final String CHARSET = ";charset=UTF-8";
	public static final float MAX_BET = 10000f;
	public static final String BET_RED = "RED";
	public static final String BET_BLACK = "BLACK";
	public static final String CREATEROULETTE = "/createRoulette";
	public static final String OPENROULETTE = "/openRoulette";
	public static final String LISTALLROULETTE = "/listAllRoulette";
	public static final String BETROULETTE = "/betRoulette";
	public static final String CLOSEROULETTE = "/closeRoulette";
}