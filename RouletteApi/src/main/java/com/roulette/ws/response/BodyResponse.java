package com.roulette.ws.response;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class BodyResponse {
	
	private String transactionId;
	private String erroMessage;
	private String responseCode;
	private String responseMessage;
	
	public BodyResponse() {
		super();
	}
	
	public BodyResponse(BodyResponse obj) {
		super();
		this.transactionId = obj.getTransactionId();
		this.responseCode = obj.getResponseCode();
		this.responseMessage = obj.getResponseMessage();
		this.erroMessage = obj.getErroMessage();
	}
	
	public BodyResponse(String transactionId, String errorMessage, String responseCode, String responseMessage) {
		super();
		this.transactionId = transactionId;
		this.erroMessage = errorMessage;
		this.responseCode = responseCode;
		this.responseMessage = responseMessage;
	}	

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public String getErroMessage() {
		return erroMessage;
	}

	public void setErroMessage(String erroMessage) {
		this.erroMessage = erroMessage;
	}

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getResponseMessage() {
		return responseMessage;
	}

	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((responseCode == null) ? 0 : responseCode.hashCode());
		result = prime * result + ((erroMessage == null) ? 0 : erroMessage.hashCode());
		result = prime * result + ((responseMessage == null) ? 0 : responseMessage.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BodyResponse other = (BodyResponse) obj;
		if (responseCode == null) {
			if (other.responseCode != null)
				return false;
		} else if (!responseCode.equals(other.responseCode))
			return false;
		if (erroMessage == null) {
			if (other.erroMessage != null)
				return false;
		} else if (!erroMessage.equals(other.erroMessage))
			return false;
		if (responseMessage == null) {
			if (other.responseMessage != null)
				return false;
		} else if (!responseMessage.equals(other.responseMessage))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "BodyResponse [errorMessage=" + erroMessage + ", responseCode=" + responseCode
				+ ", responseMessage=" + responseMessage + "]";
	}

	
}
