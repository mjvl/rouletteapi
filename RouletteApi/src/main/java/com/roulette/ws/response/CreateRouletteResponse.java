package com.roulette.ws.response;

public class CreateRouletteResponse extends BodyResponse {
	private int rouletteId;
	public int getRouletteId() {
		return rouletteId;
	}
	public void setRouletteId(int rouletteId) {
		this.rouletteId = rouletteId;
	}
}
