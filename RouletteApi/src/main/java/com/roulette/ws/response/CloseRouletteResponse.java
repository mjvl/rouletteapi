package com.roulette.ws.response;

import com.roulette.ws.model.Roulette;

public class CloseRouletteResponse extends BodyResponse {
	private Roulette roulette;
	public Roulette getRoulette() {
		return roulette;
	}
	public void setRoulette(Roulette roulette) {
		this.roulette = roulette;
	}
}
