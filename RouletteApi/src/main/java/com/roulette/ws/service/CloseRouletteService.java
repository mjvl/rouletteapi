package com.roulette.ws.service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.roulette.ws.dao.RouletteDao;
import com.roulette.ws.model.Bet;
import com.roulette.ws.model.Roulette;
import com.roulette.ws.request.CloseRouletteRequest;
import com.roulette.ws.response.CloseRouletteResponse;
import com.roulette.ws.util.Constants;
import com.roulette.ws.util.Util;

@Service
public class CloseRouletteService implements Serializable {
	private static final long serialVersionUID = 1L;
	@Autowired
	private RouletteDao rouletteDao;
	public CloseRouletteResponse closeRoulette(CloseRouletteRequest clouseRouletteRequest) throws Exception {
		CloseRouletteResponse closeRouletteResponse = new CloseRouletteResponse();
		try {
			Roulette roulette = validateRoulette(this.getRouletteById(clouseRouletteRequest.getRouletteId()));
			if(roulette==null) {
				return closeRouletteResponse;
			}
			roulette.setRouletteOpen(false);
			Bet winnerBet = this.getWinnerBet();
			roulette.setBetList(this.getBetWinners(roulette.getBetList(), winnerBet));
			boolean rouletteClosed = false;
			rouletteClosed = rouletteDao.updateRoulette(roulette);
			if(rouletteClosed) {
				closeRouletteResponse.setResponseCode("0");
				closeRouletteResponse.setResponseMessage("Roulette closed.");
				closeRouletteResponse.setRoulette(roulette);
			}
			else {
				return closeRouletteResponse;
			}
		}
		catch (Exception e) {
			closeRouletteResponse.setResponseCode("-1");
			closeRouletteResponse.setResponseMessage("Exception: " + e.getMessage());
		}
		return closeRouletteResponse;
	}
	private Roulette validateRoulette(Roulette rouletteInput) {
		Roulette rouletteOutput = new Roulette();
		if (rouletteInput != null && rouletteInput.isRouletteOpen()) {
			rouletteOutput = rouletteInput;
			if (rouletteInput.getBetList() != null) {
				return rouletteOutput;
			} else {
				List<Bet> betList = new ArrayList<Bet>();
				rouletteOutput.setBetList(betList);
				return rouletteOutput;
			}
		} else {
			return null;
		}
	}
	private Roulette getRouletteById(int rouletteId) throws Exception {
		return new Gson().fromJson(Util.printPrettyJSONString(rouletteDao.getRouletteById(rouletteId)), Roulette.class);
	}
	private Bet getWinnerBet() {
		Bet bet = new Bet();
		int winnerNumber = ThreadLocalRandom.current().nextInt(0, 36 + 1);
		bet.setBetNumber(String.valueOf(winnerNumber));
		if(winnerNumber%2!=0) {
			bet.setColor(Constants.BET_BLACK);
		}
		else {
			bet.setColor(Constants.BET_RED);
		}
		return bet;
	}
	private List<Bet> getBetWinners(List<Bet> betList, Bet winnerBet){
		for(int i=0;i<betList.size();i++) {
			if(winnerBet.getBetNumber().equals(betList.get(i).getBetNumber())) {
				float moneyBet = betList.get(i).getMoneyBet();
				betList.get(i).setMoneyBet(moneyBet*5f);
				betList.get(i).setWinner(true);
			}
			else if(winnerBet.getColor().equals(betList.get(i).getColor())) {
				float moneyBet = betList.get(i).getMoneyBet();
				betList.get(i).setMoneyBet(moneyBet*1.8f);
				betList.get(i).setWinner(true);
			}
			else {
				betList.get(i).setMoneyBet(0f);
				betList.get(i).setWinner(false);
			}
		}
		return betList;
	}
}
