package com.roulette.ws.service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.google.gson.Gson;
import com.roulette.ws.dao.RouletteDao;
import com.roulette.ws.model.Bet;
import com.roulette.ws.model.Roulette;
import com.roulette.ws.request.BetRouletteRequest;
import com.roulette.ws.response.BodyResponse;
import com.roulette.ws.util.Constants;
import com.roulette.ws.util.Util;

@Service
public class BetRouletteService implements Serializable {
	private static final long serialVersionUID = 1L;
	@Autowired
	private RouletteDao rouletteDao;
	public BodyResponse betRoulette(String userId, BetRouletteRequest betRouletteRequest) throws Exception {
		BodyResponse betRouletteResponse = new BodyResponse();
		Roulette roulette = validateRoulette(this.getRouletteById(betRouletteRequest.getRouletteId()));
		if (roulette != null) {
			try {
				Bet bet = buildBet(betRouletteRequest, roulette, userId);
				if(bet == null) {
					return betRouletteResponse;
				}
				else {
					roulette.getBetList().add(bet);
				}
				if (rouletteDao.updateRoulette(roulette)) {
					betRouletteResponse.setResponseCode("0");
					betRouletteResponse.setResponseMessage("Bet placed on roulette " + betRouletteRequest.getRouletteId());
				} else {
					betRouletteResponse.setResponseCode("1");
					betRouletteResponse.setResponseMessage("Bet unsuccesful.");
				}
			} catch (Exception e) {
				betRouletteResponse.setResponseCode("-1");
				betRouletteResponse.setResponseMessage("Exception: "+e.getMessage());
			}
		}
		return betRouletteResponse;
	}
	private Bet buildBet(BetRouletteRequest betRouletteRequest, Roulette roulette, String userId) throws Exception {
		Bet bet = new Bet();
		try {
			if (betRouletteRequest.getBetNumber() == null) {
				bet.setColor(betRouletteRequest.getColor());
			} else {
				int betNumber = Integer.parseInt(betRouletteRequest.getBetNumber());
				if (betNumber >= 0 && betNumber <= 36) {
					bet.setBetNumber(betRouletteRequest.getBetNumber());
				}
				else {
					return null;
				}
			}
			if (validateBetMoney(betRouletteRequest.getMoneyBet())) {
				bet.setMoneyBet(betRouletteRequest.getMoneyBet());
			} else {
				return null;
			}
			bet.setBetId(this.getBetId(roulette));
			bet.setUserId(Integer.parseInt(userId));
		} catch (Exception e) {
			throw new Exception();
		}

		return bet;
	}
	private boolean validateBetMoney(float money) {
		if (money > Constants.MAX_BET) {
			return false;
		} else {
			return true;
		}
	}
	private Roulette getRouletteById(int rouletteId) throws Exception {
		return new Gson().fromJson(Util.printPrettyJSONString(rouletteDao.getRouletteById(rouletteId)), Roulette.class);
	}
	private Roulette validateRoulette(Roulette rouletteInput) {
		Roulette rouletteOutput = new Roulette();
		if (rouletteInput != null && rouletteInput.isRouletteOpen()) {
			rouletteOutput = rouletteInput;
			if (rouletteInput.getBetList() != null) {
				return rouletteOutput;
			} else {
				List<Bet> betList = new ArrayList<Bet>();
				rouletteOutput.setBetList(betList);
				return rouletteOutput;
			}
		} else {
			return null;
		}
	}
	private int getBetId(Roulette roulette) {
		if(roulette.getBetList()!=null) {
			if(roulette.getBetList().isEmpty()) {
				return 1;
			}
			else {
				int lastIndex = roulette.getBetList().size()-1;
				return roulette.getBetList().get(lastIndex).getBetId()+1;
			}
		}
		else {
			return 1;
		}
		
	}
}
