package com.roulette.ws.service;

import java.io.Serializable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.roulette.ws.dao.RouletteDao;
import com.roulette.ws.model.Roulette;
import com.roulette.ws.response.CreateRouletteResponse;

@Service
public class NewRouletteService implements Serializable {
	private static final long serialVersionUID = 1L;
	@Autowired
	private RouletteDao rouletteDao;
	public CreateRouletteResponse createNewRoulette() {
		CreateRouletteResponse createRouletteResponse = new CreateRouletteResponse();
		boolean rouletteCreated = false;
		try {
			Roulette roulette = buildRoulette(rouletteDao.getLastRoulette());
			rouletteCreated = rouletteDao.createNewRoulette(roulette);
			if(rouletteCreated) {
				createRouletteResponse.setRouletteId(roulette.getRouletteId());
				createRouletteResponse.setResponseCode("0");
				createRouletteResponse.setResponseMessage("Roulette was created.");
			}
		}
		catch (Exception e) {
			createRouletteResponse.setResponseCode("-1");
			createRouletteResponse.setResponseMessage("Roulette was not created, ex: " + e.getMessage());
		}
		return createRouletteResponse;
	}
	private Roulette buildRoulette (int lastRouletteId) {
		Roulette roulette = new Roulette();
		roulette.setRouletteId(lastRouletteId+1);
		return roulette;
	}
}
