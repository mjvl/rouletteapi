package com.roulette.ws.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.roulette.ws.dao.RouletteDao;

@Service
public class ListAllRoulettesService {
	@SuppressWarnings("unused")
	private static final long serialVersionUID = 1L;
	@Autowired
	private RouletteDao rouletteDao;
	public List<Object> listAllRoulettes(){
		return rouletteDao.listAllRoulettes();
	}
	public Object getRouletteById(int rouletteId) {
		return rouletteDao.getRouletteById(rouletteId);
	}
}
