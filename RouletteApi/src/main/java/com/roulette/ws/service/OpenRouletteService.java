package com.roulette.ws.service;

import java.io.Serializable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.roulette.ws.dao.RouletteDao;
import com.roulette.ws.model.Roulette;
import com.roulette.ws.request.OpenRouletteRequest;
import com.roulette.ws.response.OpenRouletteResponse;

@Service
public class OpenRouletteService implements Serializable {
	private static final long serialVersionUID = 1L;
	@Autowired
	private RouletteDao rouletteDao;
	public OpenRouletteResponse openRoulette(OpenRouletteRequest openRouletteRequest) {
		OpenRouletteResponse openRouletteResponse = new OpenRouletteResponse();
		boolean rouletteOpen = false;
		Roulette roulette = new Roulette();
		try {
			roulette = buildRoulette(openRouletteRequest.getRouletteId());
			rouletteOpen = rouletteDao.updateRoulette(roulette);
			if(rouletteOpen) {
				openRouletteResponse.setResponseCode("0");
				openRouletteResponse.setResponseMessage("Success: Roulette opened.");
			}
			else {
				openRouletteResponse.setResponseCode("1");
				openRouletteResponse.setResponseMessage("Error: Roulette was not opened.");
			}
		}
		catch (Exception e) {
			openRouletteResponse.setResponseCode("-1");
			openRouletteResponse.setResponseMessage("Error: Roulette was not opened, ex: " + e.getMessage());
		}
		return openRouletteResponse;
	}
	private Roulette buildRoulette (int rouletteId) {
		Roulette roulette = new Roulette();
		roulette.setRouletteId(rouletteId);
		roulette.setRouletteOpen(true);
		return roulette;
	}
}
