package com.roulette.ws;

import java.util.List;

import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.roulette.ws.model.Roulette;
import com.roulette.ws.request.BetRouletteRequest;
import com.roulette.ws.request.CloseRouletteRequest;
import com.roulette.ws.request.OpenRouletteRequest;
import com.roulette.ws.response.BodyResponse;
import com.roulette.ws.response.CloseRouletteResponse;
import com.roulette.ws.response.CreateRouletteResponse;
import com.roulette.ws.response.OpenRouletteResponse;
import com.roulette.ws.service.BetRouletteService;
import com.roulette.ws.service.CloseRouletteService;
import com.roulette.ws.service.ListAllRoulettesService;
import com.roulette.ws.service.NewRouletteService;
import com.roulette.ws.service.OpenRouletteService;
import com.roulette.ws.util.Constants;

@SpringBootApplication
@RestController
@RequestMapping(Constants.PATH)
public class RouletteApiApplication {
	@Autowired
	private NewRouletteService newRouletteService;
	@Autowired
	private OpenRouletteService openRouletteService;
	@Autowired
	private ListAllRoulettesService listAllRoulettesService;
	@Autowired
	private BetRouletteService betRouletteService;
	@Autowired
	private CloseRouletteService closeRouletteService;
	@SuppressWarnings("finally")
	@RequestMapping(
			value = Constants.CREATEROULETTE, 
			method = RequestMethod.POST, 
			consumes = { MediaType.APPLICATION_JSON }, 
			produces = { MediaType.APPLICATION_JSON + Constants.CHARSET })
	@ResponseBody
	public CreateRouletteResponse createRoulette() throws Exception {
		CreateRouletteResponse createRouletteResponse = new CreateRouletteResponse();
		try {
			createRouletteResponse = newRouletteService.createNewRoulette();
		}
		catch (Exception e) {
			createRouletteResponse.setResponseMessage("Exception: "+e.getMessage());
		}
		finally {
			return createRouletteResponse;
		}
	}
	@SuppressWarnings("finally")
	@RequestMapping(
			value = Constants.OPENROULETTE, 
			method = RequestMethod.POST, 
			consumes = { MediaType.APPLICATION_JSON }, 
			produces = { MediaType.APPLICATION_JSON + Constants.CHARSET })
	@ResponseBody
	public OpenRouletteResponse openRoulette(@RequestBody OpenRouletteRequest openRouletteRequest) throws Exception {
		OpenRouletteResponse openRouletteResponse = new OpenRouletteResponse();
		try {
			openRouletteResponse = openRouletteService.openRoulette(openRouletteRequest);
		}
		catch (Exception e) {
			openRouletteResponse.setResponseMessage("Exception: "+e.getMessage());
		}
		finally {
			return openRouletteResponse;
		}
	}
	@RequestMapping(
			value = Constants.LISTALLROULETTE, 
			method = RequestMethod.POST, 
			consumes = { MediaType.APPLICATION_JSON }, 
			produces = { MediaType.APPLICATION_JSON + Constants.CHARSET })
	@ResponseBody
	public List<Roulette> listAllRoulette() throws Exception {
		@SuppressWarnings("unchecked")
		List<Roulette> listRoulette = (List<Roulette>) (Object) listAllRoulettesService.listAllRoulettes();
		return listRoulette;
	}
	@SuppressWarnings("finally")
	@RequestMapping(
			value = Constants.BETROULETTE, 
			method = RequestMethod.POST, 
			consumes = { MediaType.APPLICATION_JSON }, 
			produces = { MediaType.APPLICATION_JSON + Constants.CHARSET })
	@ResponseBody
	public BodyResponse betRoulette(@RequestHeader(value="userId") String userId, @RequestBody BetRouletteRequest betRouletteRequest) throws Exception {
		BodyResponse bodyResponse = new BodyResponse();
		try {
			bodyResponse = betRouletteService.betRoulette(userId, betRouletteRequest);
			if(bodyResponse.getResponseCode()==null) {
				bodyResponse.setResponseCode("1");
				bodyResponse.setResponseMessage("Bet unsuccesful.");
			}
		}
		catch (Exception e) {
			bodyResponse.setResponseMessage("Exception: "+e.getMessage());
		}
		finally {
			return bodyResponse;
		}
	}
	@SuppressWarnings("finally")
	@RequestMapping(
			value = Constants.CLOSEROULETTE, 
			method = RequestMethod.POST, 
			consumes = { MediaType.APPLICATION_JSON }, 
			produces = { MediaType.APPLICATION_JSON + Constants.CHARSET })
	@ResponseBody
	public CloseRouletteResponse closeRoulette(@RequestBody CloseRouletteRequest clouseRouletteRequest) throws Exception {
		CloseRouletteResponse closeRouletteResponse = new CloseRouletteResponse();
		try {
			closeRouletteResponse = closeRouletteService.closeRoulette(clouseRouletteRequest);
			if(closeRouletteResponse.getResponseCode()==null) {
				closeRouletteResponse.setResponseCode("1");
				closeRouletteResponse.setResponseMessage("Roulette not closed.");
			}
		}
		catch (Exception e) {
			closeRouletteResponse.setResponseMessage("Exception: "+e.getMessage());
		}
		finally {
			return closeRouletteResponse;
		}
	}
	public static void main(String[] args) {
		SpringApplication.run(RouletteApiApplication.class, args);
	}
}
