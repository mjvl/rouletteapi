package com.roulette.ws.request;

import java.io.Serializable;

public class BetRouletteRequest implements Serializable {
	private static final long serialVersionUID = 1L;
	private int rouletteId;
	private String betNumber;
	private String color;
	private float moneyBet;
	public int getRouletteId() {
		return rouletteId;
	}
	public void setRouletteId(int rouletteId) {
		this.rouletteId = rouletteId;
	}
	public String getBetNumber() {
		return betNumber;
	}
	public void setBetNumber(String betNumber) {
		this.betNumber = betNumber;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public float getMoneyBet() {
		return moneyBet;
	}
	public void setMoneyBet(float moneyBet) {
		this.moneyBet = moneyBet;
	}
}
