package com.roulette.ws.request;

import java.io.Serializable;

public class OpenRouletteRequest implements Serializable {
	private static final long serialVersionUID = 1L;
	private int rouletteId;
	public int getRouletteId() {
		return rouletteId;
	}
	public void setRouletteId(int rouletteId) {
		this.rouletteId = rouletteId;
	}
}
